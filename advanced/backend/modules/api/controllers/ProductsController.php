<?php


namespace backend\modules\api\controllers;

use backend\modules\api\models;

class ProductsController extends \yii\web\Controller
{
    protected $curl;
    protected $response;
    protected $token;
    protected $ShopifyURI = "https://158f96af8560d277abc51418df2c9362:06f946514f72e9122a1de00ff870b5bb@ihabz.myshopify.com/admin/";
    protected $VendURI = "https://ihabz.vendhq.com/api/products";
    protected $Vend_Client_id = "cajGis1BzldjRBRHarBjGY44VbdOL0ws";
    protected $Vend_Secret_id = "T6B0Fkn3y0eqFw2CwywINNUbj9wOWcWA";
    protected $Vend_domainPrefix = "ihabz";
    protected $Vend_redirectUri = "http://localhost/stitchlite/advanced/backend/web/api/products/sync";

    public function initSession()
    {
        @session_start();
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function makeCall($uri)
    {
        $this->initCurl($uri);

        $this->processCurl($uri);
    }

    public function initCurl($uri)
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_URL, $uri);
        curl_setopt($this->curl, CURLOPT_HTTPGET, 1);
        curl_setopt($this->curl, CURLOPT_HEADER, false);
        if (isset($_SESSION['accessToken'])) {
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $_SESSION['accessToken'], 'Accept: application/json', 'Content-Type: application/json'));

        } else {
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));

        }

        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

    }

    public function processCurl($uri)
    {
        if (ereg("^(https)", $uri)) curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);

        $this->response = curl_exec($this->curl);
        curl_close($this->curl);
    }

    public function constructURI($URI = null)
    {
        if ($URI != null) {
            $this->ShopifyURI .= $URI;
        } else {
            $this->ShopifyURI .= "/products.json";
        }

    }

    protected function _getAttachedVariant( $id )
    {
        $variants = new models\ProductVariants();
        $records = $variants::find()
            ->where(['product_id' => $id])
            ->asArray()
            ->all();

        return $records;
    }

    public function actionGetproducts( $id = null )
    {
        $product =new models\Products();


        if( $id != NULL )
        {
            $record = $product::find()
                ->where(['id' => $id])
                ->asArray()
                ->one();
            $record["variants"] = $this->_getAttachedVariant( $id );
            echo json_encode($record , 128);
        }
        else
        {
            $records = $product::find()
                ->asArray()
                ->all();

            $result = array();
            for( $i = 0 ;$i < count( $records ) ;$i++  )
            {
                $result[ $i ] = $records[$i];
                $id = $records[$i]['id'];
                $variants = $this->_getAttachedVariant( $id );
                $result[$i]["variants"] = $variants;
            }
            echo json_encode($result , 128);
        }
    }

    public function actionSync()
    {
        echo $this->_syncShopifyproducts("id,updated_at");
        echo $this->_callVend();
    }

    protected function _syncVendproducts()
    {
        $this->initSession();
        $this->makeCall($this->VendURI);



        $productModel = new models\Products();
        $productVariantModel = new models\ProductVariants();
        $products = json_decode($this->response);
        foreach ($products->products as $product) {
            if (strpos($product->handle, 'vend') !== false) {
                continue;
            }

            //no variant this mean its parent product! lets add it as parent and variant
            $record = $productModel::find()
                ->where(['product_channel_id' => $product->id])
                ->one();

            //does not exist insert
            if (empty($record)) {
                if ($product->variant_parent_id == '') {
                    $row = array();
                    $row["product_channel_id"] = $product->id;
                    $row["Name"] = $product->base_name;
                    $row["Updated_at"] = $product->updated_at;
                    $this->_insertProduct($row);
                    $id = \Yii::$app->db->getLastInsertID();

                    //insert channel product record
                    $row = array();
                    $row["product_id"] = $id;
                    //we are hardcoding channel ID for Vend
                    $row["channel_id"] = 1;
                    $row["status"] = 1;
                    $row["last_updated"] = $product->updated_at;

                    $this->_insertChannelProduct( $row );


                    //vend has variant title lets insert it if not exist
                    $Variantid = $this->_getVariantId($product->variant_option_one_name);
                    echo $Variantid;
                    //once you are done with parent product lets insert it as variant
                    $row = array();
                    $row['product_id'] = $id;
                    $row['value'] = $product->name;
                    $row['variant_id'] = $Variantid;
                    $row['variant_price'] = $product->price;
                    $row['variant_sku'] = $product->sku;
                    $row['variant_quantity'] = $product->inventory[0]->count;
                    $this->_insertProductVariant($row);
                } elseif ($product->variant_parent_id >= '') {

                    //make sure this variant doe not exist first
                    $record = $productVariantModel::find()
                        ->where(['variant_sku' => $product->sku])
                        ->one();

                    if (!empty($record)) {
                        continue;
                    }

                    ///this is a variant and it has a parent product lets get that ID to save the variants

                    $record = $productModel::find()
                        ->where(['product_channel_id' => $product->variant_parent_id])
                        ->one();

                    if (!empty($record)) {
                        $row = array();
                        $Variantid = $this->_getVariantId($product->variant_option_one_name);
                        $row['product_id'] = $record->id;
                        $row['value'] = $product->name;
                        $row['variant_id'] = $Variantid;
                        $row['variant_price'] = $product->price;
                        $row['variant_sku'] = $product->sku;
                        $row['variant_quantity'] = $product->inventory[0]->count;
                        $this->_insertProductVariant($row);
                    } else {
                        throw new exception("There was an error");
                    }
                }
            } elseif (strtotime(($record->Updated_at)) < strtotime(strtotime(($product->updated_at)))) {
                if ($product->variant_parent_id == "") {
                    echo "update<br>";

                    $record->product_channel_id = $product->id;
                    $record->Name = $product->base_name;
                    $record->Updated_at =  date("Y-m-d H:i:s", strtotime(($product->updated_at)));
                    $record->save(false);
                } else {
                    $record = $productModel::find()
                        ->where(['product_channel_id' => $product->variant_parent_id])
                        ->one();

                    $Variantid = $this->_getVariantId($product->variant_option_one_name);

                    $variantRecord = $productVariantModel::find()
                        ->where(['product_id' => $record->id])
                        ->one();
                    $variantRecord->value = $product->name;
                    $variantRecord->variant_id = $Variantid;
                    $variantRecord->variant_price = $product->price;
                    $variantRecord->variant_sku = $product->sku;
                    $variantRecord->variant_quantity = $product->inventory[0]->count;
                    $variantRecord->save(false);
                }
            }
        }

    }

    protected function _getVariantId($name)
    {
        $variantModel = new models\Variant();
        $row = array();
        $row["Name"] = $name;
        $VariantRecord = $variantModel::find()
            ->where(['Name' => $row["Name"]])
            ->one();

        if (empty($VariantRecord)) {
            $this->_insertVariant($row);
            return \Yii::$app->db->getLastInsertID();
        } else {
            return $VariantRecord->id;
        }
    }

    protected function _insertChannelProduct( $product )
    {
        $row = new models\ChannelProducts();
        $row->product_id = $product["product_id"];
        $row->channel_id = $product["channel_id"];
        $row->status = $product["status"];
        $row->last_updated = $product["last_updated"];
        $row->save(false);
    }

    protected function _insertVariant($variant)
    {
        $row = new models\Variant();
        $row->Name = $variant["Name"];
        $row->save(false);
    }

    protected function _insertProduct($product)
    {
        $row = new models\Products();
        $row->product_channel_id = $product["product_channel_id"];
        $row->Name = $product["Name"];
        $row->Updated_at = $product["Updated_at"];
        $row->save(false);
    }

    protected function _insertProductVariant($variant)
    {
        $row = new models\ProductVariants();
        $row->product_id = $variant["product_id"];
        $row->variant_id = $variant["variant_id"];
        $row->value = $variant["value"];
        $row->variant_price = $variant["variant_price"];
        $row->variant_sku = $variant["variant_sku"];
        $row->variant_quantity = $variant["variant_quantity"];
        $row->save(false);
    }

    protected function _updateProductVariant($id, $variant)
    {
        $row = new models\ProductVariants();
        $row::find()
            ->where(['id' => $id])
            ->one();
        $row->value = $variant["value"];
        $row->variant_price = $variant["variant_price"];
        $row->variant_sku = $variant["variant_sku"];
        $row->variant_quantity = $variant["variant_quantity"];
        $row->save(true);
    }

    protected function _checkShopifyProducts($products)
    {
        $productModel = new models\Products();
        $variantMode = new models\ProductVariants();
        foreach ($products->products as $product) {
            $record = $productModel::find()
                ->where(['product_channel_id' => $product->id])
                ->one();
            if (empty($record)) {
                echo "insert<br>";
                $row = array();
                $row["product_channel_id"] = $product->id;
                $row["Name"] = $product->title;
                $row["Updated_at"] = $product->updated_at;
                $this->_insertProduct($row);
                $id = \Yii::$app->db->getLastInsertID();


                //insert channel product record
                $row = array();
                $row["product_id"] = $id;
                //we are hardcoding channel ID for shopify
                $row["channel_id"] = 2;
                $row["status"] = 1;
                $row["last_updated"] = $product->updated_at;

                $this->_insertChannelProduct( $row );



                //lets insert Variants
                foreach ($product->variants as $variant) {
                    $row = array();
                    $Variantid = $this->_getVariantId($variant->title);
                    $row['product_id'] = $id;
                    $row['variant_id'] = $Variantid;
                    $row['value'] = $variant->option1;
                    $row['variant_price'] = $variant->price;
                    $row['variant_sku'] = $variant->sku;
                    $row['variant_quantity'] = $variant->inventory_quantity;
                    $this->_insertProductVariant($row);
                }
            } //Channel has more recent record updated lets update ours
            elseif (strtotime(($record->Updated_at)) < strtotime(date("Y-m-d H:i:s", strtotime(($product->updated_at)) - 32400))) {
                echo "update<br>";
                $record->product_channel_id = $product->id;
                $record->Name = $product->title;
                $record->Updated_at = date("Y-m-d H:i:s", strtotime(($product->updated_at)) - 32400);
                $record->save(false);

                foreach ($product->variants as $variant) {

                    $variantRecord = $variantMode::find()
                        ->where(['product_id' => $record->id])
                        ->one();
                    $variantRecord->value =  $variant->option1;
                    $variantRecord->variant_price = $variant->price;
                    $variantRecord->variant_sku = $variant->sku;
                    $variantRecord->variant_quantity = $variant->inventory_quantity;
                    $variantRecord->save(true);

                }

            } else {
                echo ( int )strtotime($record->Updated_at) . "  " . ( int )strtotime(str_replace("T", " ", $product->updated_at))
                    . "  ";
            }


        }

    }

    protected function _getShopifyProduct($URI)
    {
        $this->constructURI($URI);
        $this->makeCall($this->ShopifyURI);

        return (json_decode($this->response));
    }

    protected function _syncShopifyproducts($fields, $id = null)
    {
        if ($id != null) {
            $this->constructURI("/products/{" . $id . "}.json");
        } else {
            $this->constructURI();
        }

        $this->makeCall($this->ShopifyURI);


        $products = (json_decode($this->response));
        $this->_checkShopifyProducts($products);
    }

    protected function _callVend()
    {
        $this->initSession();
        $provider = new \Wheniwork\OAuth2\Client\Provider\Vend([
            'clientId' => $this->Vend_Client_id,
            'clientSecret' => $this->Vend_Secret_id,
            'domainPrefix' => $this->Vend_domainPrefix,
            'redirectUri' => $this->Vend_redirectUri
        ]);

        if (!isset($_GET['code'])) {

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl();
            $_SESSION['oauth2state'] = $provider->state;
            header('Location: ' . $authUrl);
            exit;

        } elseif (isset($_SESSION['accessToken'])) {
            $this->_syncVendproducts();

// Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {


            exit($_SESSION['oauth2state'] . ' Invalid state');

        } else {

            // Try to get an access token (using the authorization code grant)
            $this->token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);

            // Vend does not provide a way to get information about the currently
            // authenticated user. (If you know of a way, please let me know!)

            // Use this to interact with an API on the users behalf
            $_SESSION['accessToken'] = $this->token->accessToken;
            $this->_syncVendproducts();
        }
    }
}
