<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $product_channel_id
 * @property string $Name
 * @property string $SKU
 * @property integer $Qunatity
 * @property double $Price
 * @property string $Updated_at
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_channel_id', 'Name'], 'required'],
            [['Qunatity'], 'integer'],
            [['Price'], 'number'],
            [['Updated_at'], 'safe'],
            [['product_channel_id', 'Name', 'SKU'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_channel_id' => 'Product Channel ID',
            'Name' => 'Name',
            'SKU' => 'Sku',
            'Qunatity' => 'Qunatity',
            'Price' => 'Price',
            'Updated_at' => 'Updated At',
        ];
    }
}
