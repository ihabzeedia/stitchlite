<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "customer_channels".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $channel_id
 */
class CustomerChannels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_channels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'channel_id'], 'required'],
            [['customer_id', 'channel_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'channel_id' => 'Channel ID',
        ];
    }
}
