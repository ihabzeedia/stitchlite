<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "channels".
 *
 * @property integer $id
 * @property string $Name
 * @property string $URL
 */
class Channels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'channels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'URL'], 'required'],
            [['Name', 'URL'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'URL' => 'Url',
        ];
    }
}
