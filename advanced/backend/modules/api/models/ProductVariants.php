<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "product_variants".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $variant_id
 * @property string $value
 * @property double $variant_price
 * @property string $variant_sku
 * @property integer $variant_quantity
 */
class ProductVariants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_variants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'value', 'variant_price', 'variant_sku', 'variant_quantity'], 'required'],
            [['product_id', 'variant_id', 'variant_quantity'], 'integer'],
            [['variant_price'], 'number'],
            [['value', 'variant_sku'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'variant_id' => 'Variant ID',
            'value' => 'Value',
            'variant_price' => 'Variant Price',
            'variant_sku' => 'Variant Sku',
            'variant_quantity' => 'Variant Quantity',
        ];
    }
}
