<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "channel_products".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $channel_id
 * @property integer $status
 * @property string $last_updated
 */
class ChannelProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'channel_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'channel_id'], 'required'],
            [['product_id', 'channel_id', 'status'], 'integer'],
            [['last_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'channel_id' => 'Channel ID',
            'status' => 'Status',
            'last_updated' => 'Last Updated',
        ];
    }
}
